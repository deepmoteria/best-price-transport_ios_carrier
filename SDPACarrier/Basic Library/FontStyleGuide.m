//
//  FontStyleGuide.m
//  
//
//  Created by Sapana Ranipa on 23/12/15.
//
//

#import "FontStyleGuide.h"

@implementation FontStyleGuide


 // For Color

+(UIColor *)ColorDefault
{
    return [UIColor colorWithRed:(0.0/255.0f) green:(84.0/255.0f) blue:(107.0/255.0f) alpha:1];
}

// For Font
+(UIFont *)fontRegularLight
{
    return [UIFont fontWithName:@"AvenirLTStd-Light" size:15.0f];
}
+(UIFont *)fontRegular
{
    return [UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
}
+(UIFont *)fontRegular:(CGFloat)size
{
    return [UIFont fontWithName:@"AvenirLTStd-Medium" size:size];
}
+(UIFont *)fontRegularBold
{
    return [UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
}
+(UIFont *)fontRegularBold:(CGFloat)size
{
    return [UIFont fontWithName:@"AvenirLTStd-Black" size:size];
}
@end
