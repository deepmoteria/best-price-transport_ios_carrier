//
//  NSPref.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPref : NSObject

+(void)SetPreference:(NSString *)key Value:(id)value;
+(id)GetPreference:(NSString *)key;
+(void)RemovePreference:(NSString *)key;
+(void)SetBoolPreference:(NSString *)key  Value:(BOOL)value;
+(BOOL)GetBoolPreference:(NSString *)key;
@end
