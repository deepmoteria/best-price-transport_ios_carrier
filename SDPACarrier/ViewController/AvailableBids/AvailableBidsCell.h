//
//  AvailableBidsCell.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 08/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvailableBidsCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleTypeYear;
@property (weak, nonatomic) IBOutlet UIButton *btnViewDetail;

@end
