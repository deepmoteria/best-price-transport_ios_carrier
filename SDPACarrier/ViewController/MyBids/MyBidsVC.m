//
//  MyBidsVC.m
//  SDPACarrier
//
//  Created by Sapana Ranipa on 08/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "MyBidsVC.h"
#import "OnGoingJobsVC.h"
#import "MenuCollectionViewCell.h"

#define ACCEPTABLE_CHARECTERS @"0123456789."

@interface MyBidsVC ()
{
    NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier;
    NSMutableArray *arrForAllBids,*arrForAcceptedBids,*arrForPendingBids;
    NSString *type,*strPickupTime,*strForDateTime;
    NSMutableDictionary *selectedBidData;
    NSTimer *TimerForGetBids;
    int buttonTag;
}
@end

@implementation MyBidsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForAllBids=[[NSMutableArray alloc] init];
    arrForAcceptedBids=[[NSMutableArray alloc] init];
    arrForPendingBids=[[NSMutableArray alloc] init];
    selectedBidData=[[NSMutableDictionary alloc] init];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForSegueIdentifier=[[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    [self GetAllBids];
    TimerForGetBids=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(GetAllBids) userInfo:nil repeats:YES];
    self.viewForBids.hidden=YES;
    self.viewForTimePicker.hidden=YES;
    
    self.textViewForDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
    self.textViewForDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_AVAILABLEBIDS", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_BIDS", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_mybids",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc]initWithObjects:@"HOME",SEGUE_TO_PROFILE,SEGUE_TO_MYBIDS,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"Logout", nil];
    
    self.viewObj=nil;
    for (int i=0; i<self.navigationController.viewControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if([vc isKindOfClass:[AvailableBidsVC class]])
        {
            self.viewObj=(AvailableBidsVC *)vc;
        }
    }
    [self ManagePush];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [TimerForGetBids invalidate];
    TimerForGetBids=nil;
    // [UIView animateWithDuration:0.5 animations:^{
    [self.btnMenu setTitle:NSLocalizedString(@"MY_BIDS", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"MY_BIDS", nil) forState:UIControlStateSelected];
    [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
}
-(void)ManagePush
{
    if(pushId==0 || pushId==1)
    {
        [self onClickBtnAcceptedBid:self.btnAcceptedBids];
        pushId=0;
    }
    else
    {
        [self onClickBtnRejectedBids:self.btnRejectedBids];
        pushId=0;
    }
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    [self.btnMenu setTitle:NSLocalizedString(@"MY_BIDS", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"MY_BIDS", nil) forState:UIControlStateSelected];
    
    [self.btnAcceptedBids setTitle:NSLocalizedString(@"BTN_ACCEPT_TITLE", nil) forState:UIControlStateNormal];
    [self.btnAcceptedBids setTitle:NSLocalizedString(@"BTN_ACCEPT_TITLE", nil) forState:UIControlStateSelected];
    
    [self.btnRejectedBids setTitle:NSLocalizedString(@"BTN_REJECT_TITLE", nil) forState:UIControlStateNormal];
    [self.btnRejectedBids setTitle:NSLocalizedString(@"BTN_REJECT_TITLE", nil) forState:UIControlStateSelected];
    
    // viewforbids
    self.lblPrice.text=NSLocalizedString(@"PRICE_YOU_CHAREGE", nil);
    self.lblPickupTime.text=NSLocalizedString(@"PICKUP_TIME", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblPickupDate.text=NSLocalizedString(@"PICKUP_DATE", nil);
    self.lblDeliverDate.text=NSLocalizedString(@"DELIVERY_DATE", nil);
    
    [self.btnCloseOfBidView setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnCloseOfBidView setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateSelected];
    [self.btnSubmitBid setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self.btnSubmitBid setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateSelected];
    [self.btnTimePickCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateNormal];
    [self.btnTimePickCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateSelected];
    [self.btnTimePickDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateNormal];
    [self.btnTimePickDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateSelected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - UITextView Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.viewForTimePicker.hidden=YES;
    if([self.textViewForDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
    {
        self.textViewForDescription.text=@"";
        self.textViewForDescription.textColor=[UIColor blackColor];
    }
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self.view endEditing:YES];
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
        self.textViewForDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    }
    return YES;
}

#pragma mark -
#pragma mark - UITextFields Delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if (textField==self.txtPrice)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.viewForTimePicker.hidden=YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:SEGUE_TO_ONGOING_BID])
    {
        OnGoingJobsVC *obj=[segue destinationViewController];
        obj.ClientData=sender;
    }
}


#pragma mark -
#pragma mark - UIButton Action Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    //[self.navigationController popViewControllerAnimated:YES];
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MY_BIDS", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MY_BIDS", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
}
- (IBAction)onClickBtnAcceptedBid:(id)sender
{
    type=@"1";
    
    [self.btnAcceptedBids setBackgroundImage:[UIImage imageNamed:@"bg_button_selected"] forState:UIControlStateNormal];
    [self.btnRejectedBids setBackgroundImage:[UIImage imageNamed:@"bg_button_not_selected"] forState:UIControlStateNormal];
    
    [self.btnAcceptedBids setTitleColor:[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1] forState:UIControlStateNormal];
    [self.btnRejectedBids setTitleColor:[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1] forState:UIControlStateNormal];
    
    [self.tableObj reloadData];
}
- (IBAction)onClickBtnRejectedBids:(id)sender
{
    type=@"2";
    
    [self.btnRejectedBids setBackgroundImage:[UIImage imageNamed:@"bg_button_selected"] forState:UIControlStateNormal];
    [self.btnAcceptedBids setBackgroundImage:[UIImage imageNamed:@"bg_button_not_selected"] forState:UIControlStateNormal];
    
    [self.btnRejectedBids setTitleColor:[UIColor colorWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1] forState:UIControlStateNormal];
    [self.btnAcceptedBids setTitleColor:[UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1] forState:UIControlStateNormal];
    
    [self.tableObj reloadData];
}
- (IBAction)OnClickBtnbtnCloseOfBidView:(id)sender
{
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForBids.hidden=YES;
}
- (IBAction)onClickBtnTime:(id)sender
{
    [self.btnTime setTitle:@"" forState:UIControlStateNormal];
    [self.btnTime setTitle:@"" forState:UIControlStateSelected];
    self.btnTime.titleLabel.text = @"";
    strPickupTime = @"";
    strForDateTime = @"Time";
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=NO;
    self.datePickerObj.datePickerMode = UIDatePickerModeTime;
    self.datePickerObj.minimumDate=[NSDate date];
    [self.datePickerObj setDate:[NSDate date]];
    [self.datePickerObj reloadInputViews];
}
- (IBAction)onClickSelectDate:(id)sender
{
    strForDateTime = @"Date";
    UIButton *btn = (UIButton *)sender;
    
    if(btn == self.btnPickupDate)
    {
        [self.btnPickupDate setTitle:@"" forState:UIControlStateNormal];
        [self.btnPickupDate setTitle:@"" forState:UIControlStateSelected];
        self.btnPickupDate.titleLabel.text = @"";
        buttonTag=1;
        self.datePickerObj.minimumDate=[NSDate date];
        [self.datePickerObj setDate:[NSDate date]];
    }
    else
    {
        [self.btnDeliveryDate setTitle:@"" forState:UIControlStateNormal];
        [self.btnDeliveryDate setTitle:@"" forState:UIControlStateSelected];
        self.btnDeliveryDate.titleLabel.text = @"";
        buttonTag=2;
        if(self.btnPickupDate.titleLabel.text.length>1)
        {
            NSDate *btnDate = [[UtilityClass sharedObject]stringToDate:self.btnPickupDate.titleLabel.text withFormate:@"MM/dd/yyyy"];
            self.datePickerObj.minimumDate=btnDate;
            [self.datePickerObj setDate:btnDate];
        }
        else
        {
            self.datePickerObj.minimumDate=[NSDate date];
            [self.datePickerObj setDate:[NSDate date]];
        }
    }
    
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForTimePicker.hidden=NO;
    self.datePickerObj.datePickerMode = UIDatePickerModeDate;
    [self.datePickerObj reloadInputViews];
}
- (IBAction)onClickBtnTimePickDone:(id)sender
{
    if([strForDateTime isEqualToString:@"Time"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        strPickupTime=[dateFormatter stringFromDate:self.datePickerObj.date];
        
        [dateFormatter setDateFormat:@"hh:mm a"];
        NSString *strForTime =[dateFormatter stringFromDate:self.datePickerObj.date];
        [self.btnTime setTitle:strForTime forState:UIControlStateNormal];
        [self.btnTime setTitle:strForTime forState:UIControlStateSelected];
    }
    else if([strForDateTime isEqualToString:@"Date"])
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *date = [dateFormat stringFromDate:self.datePickerObj.date];
        
        if(buttonTag == 1)
        {
            [self.btnPickupDate setTitle:date forState:UIControlStateNormal];
            [self.btnPickupDate setTitle:date forState:UIControlStateSelected];
        }
        else if(buttonTag==2)
        {
            [self.btnDeliveryDate setTitle:date forState:UIControlStateNormal];
            [self.btnDeliveryDate setTitle:date forState:UIControlStateSelected];
        }
    }
    
    self.viewForTimePicker.hidden=YES;
    
}
- (IBAction)onClickBtnTimePickCancel:(id)sender
{
    self.viewForTimePicker.hidden=YES;
}
- (IBAction)onClickBtnbtnSubmitBid:(id)sender
{
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtPrice.text.length<1 /*|| self.btnPickupDate.titleLabel.text.length<1 ||self.btnTime.titleLabel.text.length<1 || self.btnDeliveryDate.titleLabel.text.length<1*/)
        {
            if(self.txtPrice.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"27", nil)];
            }
            /*else if(self.btnPickupDate.titleLabel.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PICKUP_TIME_REQUIRED", nil)];
            }
            else if(self.btnTime.titleLabel.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"BID_TIME_REQUIRED", nil)];
            }
            else if(self.btnDeliveryDate.titleLabel.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"DELIVERY_TIME_REQUIRED", nil)];
            }*/
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"BIDING", nil)];
            
            NSString *userID=[NSPref GetPreference:PREF_ID];
            NSString *userToken=[NSPref GetPreference:PREF_TOKEN];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            
            [dictParam setObject:userToken forKey:PARAM_TOKEN];
            [dictParam setObject:userID forKey:PARAM_ID];
            [dictParam setObject:[selectedBidData valueForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
            [dictParam setObject:self.txtPrice.text forKey:PARAM_BID_PRICE];
            
            if(strPickupTime.length>1)
            {
                if([strPickupTime isEqualToString:@"00:00:00"])
                {
                    [dictParam setObject:@"00:00:00" forKey:PARAM_BID_TIME];
                }
                else
                {
                    [dictParam setObject:[self OnlyTimeConverter:strPickupTime] forKey:PARAM_BID_TIME];
                }
            }
            else
            {
                [dictParam setObject:@"00:00:00" forKey:PARAM_BID_TIME];
            }
            
            if(self.btnPickupDate.titleLabel.text.length>1)
            {
                if([self.btnPickupDate.titleLabel.text isEqualToString:@"0000-00-00"])
                {
                    [dictParam setObject:@"0000-00-00" forKey:PARAM_BID_PICKUP_DATE];
                }
                else
                {
                    [dictParam setObject:[self DateConverter1:self.btnPickupDate.titleLabel.text] forKey:PARAM_BID_PICKUP_DATE];
                }
            }
            else
            {
                [dictParam setObject:@"0000-00-00" forKey:PARAM_BID_PICKUP_DATE];
            }
            
            if(self.btnDeliveryDate.titleLabel.text.length>1)
            {
                if([self.btnDeliveryDate.titleLabel.text isEqualToString:@"0000-00-00 00:00:00"])
                {
                    [dictParam setObject:@"0000-00-00 00:00:00" forKey:PARAM_BID_DELIVERY_DATE];
                }
                else
                {
                    [dictParam setObject:[self DateConverter:self.btnDeliveryDate.titleLabel.text] forKey:PARAM_BID_DELIVERY_DATE];
                }
            }
            else
            {
                [dictParam setObject:@"0000-00-00 00:00:00" forKey:PARAM_BID_DELIVERY_DATE];
            }
            
            [dictParam setObject:[selectedBidData valueForKey:@"client_id"] forKey:PARAM_CLIENT_ID];
            if([self.textViewForDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
            {
                [dictParam setObject:@" " forKey:PARAM_DESCRIPTION];
            }
            else
            {
                [dictParam setObject:self.textViewForDescription.text forKey:PARAM_DESCRIPTION];
            }
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_BIDDING_ON_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                     if([[response valueForKey:@"success"] boolValue])
                     {
                         NSLog(@"Bid response ------ > %@",response);
                         [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"BID_SUCCESS", nil)];
                         self.viewForTimePicker.hidden=YES;
                         self.viewForBids.hidden=YES;
                         [self GetAllBids];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

#pragma mark -
#pragma mark - WebServices Methods

-(void)GetAllBids
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        //[[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"GETTING_BIDS", nil)];
        
        NSString *userID=[NSPref GetPreference:PREF_ID];
        NSString *userToken=[NSPref GetPreference:PREF_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:userID forKey:PARAM_ID];
        [dictParam setObject:userToken forKey:PARAM_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_GET_BIDS withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [arrForAllBids removeAllObjects];
                     [arrForAllBids addObjectsFromArray:[response valueForKey:@"all_bidding"]];
                     [self SetAcceptAndPendingArray];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
             
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)SetAcceptAndPendingArray
{
    [arrForAcceptedBids removeAllObjects];
    [arrForPendingBids removeAllObjects];
    
    for (int i=0; i<arrForAllBids.count; i++)
    {
        NSMutableDictionary *dict=[arrForAllBids objectAtIndex:i];
        if([[dict valueForKey:@"is_accepted"] isEqualToString:@"1"])
        {
            [arrForAcceptedBids addObject:[arrForAllBids objectAtIndex:i]];
        }
        else
        {
            [arrForPendingBids addObject:[arrForAllBids objectAtIndex:i]];
        }
    }
    [self.tableObj reloadData];
}

#pragma mark -
#pragma mark - UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([type isEqualToString:@"1"])
    {
        return arrForAcceptedBids.count;
    }
    else if([type isEqualToString:@"2"])
    {
        return arrForPendingBids.count;
    }
    else
    {
        return 0;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyBidsCell *cell=(MyBidsCell *)[self.tableObj dequeueReusableCellWithIdentifier:@"MyBids" forIndexPath:indexPath];
    
    if([type isEqualToString:@"1"])
    {
        cell.lblYouBided.hidden=YES;
        cell.btnMessage.enabled=NO;
        [cell.btnMessage setTitleColor:[UIColor colorWithRed:54.0/255.0f green:137.0/255.0f blue:79.0/255.0f alpha:1] forState:UIControlStateNormal];
        
        NSMutableDictionary *dictData=[arrForAcceptedBids objectAtIndex:indexPath.row];
        
        cell.lblUserName.text=NSLocalizedString(@"VEHICLE", nil);
        cell.lblCarTypeYear.text=[NSString stringWithFormat:@"%@ %@ : %@, %@",[dictData valueForKey:@"make"],[dictData valueForKey:@"model"],[dictData valueForKey:@"year"],[dictData valueForKey:@"trim"]];
        
        NSString *strForPickupTime = [dictData valueForKey:@"pickup_time"];
        
        NSString *strForPickupDate = [dictData valueForKey:@"pickup_date"];
        
        NSString *strForDeliveryTime = [dictData valueForKey:@"delivery_time"];
        
        if(![strForPickupDate isEqualToString:@"0000-00-00"])
        {
            if(![strForPickupTime isEqualToString:@"00:00:00"])
            {
                NSString *str=[self TimeConverter:[NSString stringWithFormat:@"%@ %@",[dictData valueForKey:@"pickup_date"],[dictData valueForKey:@"pickup_time"]]];
                [cell.btnMessage setTitle:[NSString stringWithFormat:@"Pick up Time : %@",str] forState:UIControlStateNormal];
                cell.btnMessage.titleLabel.text=[NSString stringWithFormat:@"Pick up Time : %@",str];
            }
            else
            {
                NSString *str=[self TimeConverter2:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"pickup_date"]]];
                [cell.btnMessage setTitle:[NSString stringWithFormat:@"Pick up Time : %@",str] forState:UIControlStateNormal];
                cell.btnMessage.titleLabel.text=[NSString stringWithFormat:@"Pick up Time : %@",str];
            }
        }
        else
        {
            if(![strForPickupTime isEqualToString:@"00:00:00"])
            {
                NSString *str=[self TimeConverter1:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"pickup_time"]]];
                [cell.btnMessage setTitle:[NSString stringWithFormat:@"Pick up Time : %@",str] forState:UIControlStateNormal];
                cell.btnMessage.titleLabel.text=[NSString stringWithFormat:@"Pick up Time : %@",str];
            }
            else
            {
                [cell.btnMessage setTitle:@"Pick up Time : N/A" forState:UIControlStateNormal];
                cell.btnMessage.titleLabel.text=@"Pick up Time : N/A";
            }
        }
        
        cell.lblPrice.text=[NSString stringWithFormat:@"$%@",[dictData valueForKey:@"bid_price"]];
        cell.lblYouBided.text=NSLocalizedString(@"BIDED", nil);
        
    }
    else if([type isEqualToString:@"2"])
    {
        cell.lblYouBided.hidden=NO;
        [cell.btnMessage setTitleColor:[UIColor colorWithRed:202.0/255.0f green:65.0/255.0f blue:71.0/255.0f alpha:1] forState:UIControlStateNormal];
        
        NSMutableDictionary *dictData=[arrForPendingBids objectAtIndex:indexPath.row];
        if([[dictData valueForKey:@"is_accepted"] isEqualToString:@"0"])
        {
            cell.btnMessage.enabled=NO;
            [cell.btnMessage setTitle:NSLocalizedString(@"PENDING_MSG_0", nil) forState:UIControlStateNormal];
        }
        else if([[dictData valueForKey:@"is_accepted"] isEqualToString:@"4"])
        {
            cell.btnMessage.enabled=NO;
            [cell.btnMessage setTitle:NSLocalizedString(@"PENDING_MSG_4", nil) forState:UIControlStateNormal];
        }
        else
        {
            cell.btnMessage.enabled=YES;
            cell.btnMessage.tag=indexPath.row;
            cell.btnMessage.userInteractionEnabled=NO;
            [cell.btnMessage addTarget:self action:@selector(BidAgain:) forControlEvents:UIControlEventTouchUpInside];
            if([[dictData valueForKey:@"is_accepted"] isEqualToString:@"2"])
            {
                [cell.btnMessage setTitle:NSLocalizedString(@"PENDING_MSG_2", nil) forState:UIControlStateNormal];
            }
            else if([[dictData valueForKey:@"is_accepted"] isEqualToString:@"3"])
            {
                [cell.btnMessage setTitle:NSLocalizedString(@"PENDING_MSG_3", nil) forState:UIControlStateNormal];
            }
        }
        
        cell.lblUserName.text=NSLocalizedString(@"VEHICLE", nil);
        cell.lblCarTypeYear.text=[NSString stringWithFormat:@"%@ %@ : %@, %@",[dictData valueForKey:@"make"],[dictData valueForKey:@"model"],[dictData valueForKey:@"year"],[dictData valueForKey:@"trim"]];
        
        cell.lblPrice.text=[NSString stringWithFormat:@"$%@",[dictData valueForKey:@"bid_price"]];
        cell.lblYouBided.text=NSLocalizedString(@"BIDED", nil);
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([type isEqualToString:@"1"])
    {
        [TimerForGetBids invalidate];
        TimerForGetBids=nil;
        NSMutableDictionary *dict=[arrForAcceptedBids objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:SEGUE_TO_ONGOING_BID sender:dict];
    }
    else if([type isEqualToString:@"2"])
    {
        NSMutableDictionary *dictData=[arrForPendingBids objectAtIndex:indexPath.row];
        if([[dictData valueForKey:@"is_accepted"] isEqualToString:@"0"] || [[dictData valueForKey:@"is_accepted"] isEqualToString:@"4"])
        {
            
        }
        else
        {
            MyBidsCell *cell=[self.tableObj cellForRowAtIndexPath:indexPath];
            UIButton *btn=cell.btnMessage;
            [self BidAgain:btn];
        }
        
    }
}
-(void)BidAgain:(id)Sender
{
    UIButton *btn=(UIButton *)Sender;
    selectedBidData=[[NSMutableDictionary alloc] init];
    selectedBidData=[arrForPendingBids objectAtIndex:btn.tag];
    
    self.txtPrice.text=[selectedBidData valueForKey:@"bid_price"];
    
    strPickupTime = [selectedBidData valueForKey:@"pickup_time"];
    
    if(![strPickupTime isEqualToString:@"00:00:00"])
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:strPickupTime];
        strPickupTime=[dateFormat stringFromDate:date];
        NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
        [dateFormatters setDateFormat:@"hh:mm a"];
        [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
        NSString *str=[dateFormatters stringFromDate:date];
        [self.btnTime setTitle:str forState:UIControlStateNormal];
    }
    else
    {
        [self.btnTime setTitle:@"" forState:UIControlStateNormal];
    }
    
    self.textViewForDescription.text=[selectedBidData valueForKey:@"description"];
    
    NSString *strForPickupDate = [selectedBidData valueForKey:@"pickup_date"];
    
    NSString *strForDeliveryDate = [selectedBidData valueForKey:@"delivery_time"];
    
    if(![strForPickupDate isEqualToString:@"0000-00-00"])
    {
        [self.btnPickupDate setTitle:[self ReverseDateConverter1:strForPickupDate] forState:UIControlStateNormal];
        [self.btnPickupDate setTitle:[self ReverseDateConverter1:strForPickupDate] forState:UIControlStateHighlighted];
    }
    else
    {
        [self.btnPickupDate setTitle:@"" forState:UIControlStateNormal];
        [self.btnPickupDate setTitle:@"" forState:UIControlStateHighlighted];
    }
    
    if(![strForDeliveryDate isEqualToString:@"0000-00-00 00:00:00"])
    {
        [self.btnDeliveryDate setTitle:[self ReverseDateConverter:strForDeliveryDate] forState:UIControlStateNormal];
        [self.btnDeliveryDate setTitle:[self ReverseDateConverter:strForDeliveryDate] forState:UIControlStateHighlighted];
    }
    else
    {
        [self.btnDeliveryDate setTitle:@"" forState:UIControlStateNormal];
        [self.btnDeliveryDate setTitle:@"" forState:UIControlStateHighlighted];
    }
    
    self.viewForBids.hidden=NO;
}

#pragma mark -
#pragma mark - Helper Methods

-(NSString *)TimeConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    // dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate: date];
    //  dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)TimeConverter2:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr=[dateFormatters stringFromDate:date];
    //  dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"yyyy-MM-dd"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)ReverseDateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)ReverseDateConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)OnlyTimeConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if([dateStr containsString:@" "])
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    else
    {
        [dateFormat setDateFormat:@"HH:mm:ss"];
    }
    
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"HH:mm"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

#pragma mark -
#pragma mark - UICollectionView Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    switch (indexPath.row)
    {
        case 0:
            [self.navigationController popToViewController:self.viewObj animated:YES];
            break;
        case 1:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self GetAllBids];
            [self onClickBtnMenu:self.btnMenu];
            break;
        case 3:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)LogoutService
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [self RemovePreferences];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self LogoutService];
                break;
            default:
                break;
        }
    }
}

@end
