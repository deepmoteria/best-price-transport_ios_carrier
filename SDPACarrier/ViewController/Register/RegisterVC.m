//
//  RegisterVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 24/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "RegisterVC.h"

@interface RegisterVC ()

@end

@implementation RegisterVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"cb_glossy_off.png"] forState:UIControlStateNormal];
    self.btnRegister.enabled=FALSE;
    [self.imgProPic applyRoundedCornersFull];
    isProPicAdded=NO;
    self.viewForArroved.hidden=YES;
    self.viewForOtp.hidden=YES;
    [self SetLocalization];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    [self.ScrollObj setContentSize:CGSizeMake(self.view.frame.size.width,755)];
    self.lblUploadPicture.text=NSLocalizedString(@"UPLOAD_PICTURE_FROM", nil);
    self.lblApproveMessage.text=NSLocalizedString(@"APPROVED_MESSAGE", nil);
    
    self.txtName.placeholder=NSLocalizedString(@"FULL_NAME", nil);
    self.txtUserName.placeholder=NSLocalizedString(@"USER_NAME", nil);
    self.txtEmail.placeholder=NSLocalizedString(@"EMAIL", nil);
    self.txtPassword.placeholder=NSLocalizedString(@"PASSWORD", nil);
    self.txtConfirmPassword.placeholder=NSLocalizedString(@"CONFIRM_PASSWORD",nil);
    self.txtMobile.placeholder=NSLocalizedString(@"MOBILE", nil);
    self.txtLicenseNumber.placeholder=NSLocalizedString(@"LICENCE_NUMBER", nil);
    self.txtPlateNumber.placeholder=NSLocalizedString(@"PLATE_NUMBER", nil);
    self.txtOtp.placeholder=NSLocalizedString(@"OTP", nil);
    
    [self.btnCamera setTitle:NSLocalizedString(@"CAMERA", nil) forState:UIControlStateNormal];
    [self.btnCamera setTitle:NSLocalizedString(@"CAMERA", nil) forState:UIControlStateSelected];
    [self.btnGallery setTitle:NSLocalizedString(@"GALLERY", nil) forState:UIControlStateNormal];
    [self.btnGallery setTitle:NSLocalizedString(@"GALLERY", nil) forState:UIControlStateSelected];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateSelected];
    [self.btnBack setTitle:NSLocalizedString(@"BTN_BACK", nil) forState:UIControlStateNormal];
    [self.btnBack setTitle:NSLocalizedString(@"BTN_BACK", nil) forState:UIControlStateSelected];
    [self.btnOK setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
    [self.btnOK setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateSelected];
    
    // Set color
    [self.txtName setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtUserName setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtEmail setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtPassword setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtConfirmPassword setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMobile setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtPlateNumber setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtLicenseNumber setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtOtp setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    // setTermAndCondition
    
    self.lblIAgree.text=NSLocalizedString(@"I_AGREE", nil);
    self.lblOtp.text=NSLocalizedString(@"ENTER_OTP", nil);
    self.lblOtpMessage.text=NSLocalizedString(@"OTP_MESSAGE", nil);
    
    CGFloat width=self.lblIAgree.intrinsicContentSize.width+7;
    CGRect BtnFrame=self.btnTermAndCondition.frame;
    BtnFrame.origin.x=self.lblIAgree.frame.origin.x+width;
    
    [self.btnTermAndCondition setTitle:NSLocalizedString(@"TERM_CONDITION", nil) forState:UIControlStateNormal];
    [self.btnTermAndCondition setTitle:NSLocalizedString(@"TERM_CONDITION", nil) forState:UIControlStateSelected];
    
    self.btnTermAndCondition.frame=BtnFrame;
    [self.btnTermAndCondition sizeToFit];
    
    self.lblLine.frame=CGRectMake(self.btnTermAndCondition.frame.origin.x, self.lblLine.frame.origin.y, self.btnTermAndCondition.frame.size.width, self.lblLine.frame.size.height);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark -
#pragma mark - UIButton Action Methods

- (IBAction)onClickSubmitOtp:(id)sender
{
    if([APPDELEGATE connected])
    {
        if(self.txtOtp.text.length>1)
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"VERIFING_NUMBER", nil)];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
            [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:self.txtOtp.text forKey:PARAM_OTP];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            {
                [afn getDataFromPath:P_OTP withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if(response)
                         [APPDELEGATE hideLoadingView];
                     {
                         response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                         
                         if([[response valueForKey:@"success"] boolValue])
                         {
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"REGISTER_SUCCESS", nil)];
                             self.viewForOtp.hidden=YES;
                             self.viewForArroved.hidden=NO;
                         }
                         else
                         {
                             NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                             [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                         }
                         
                         self.txtOtp.text = @"";
                     }
                 }];
            }
        }
        else
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_OTP", nil)];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

- (IBAction)onClickBtnRegister:(id)sender
{
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtName.text.length<1 || self.txtUserName.text.length<1 || self.txtEmail.text.length<1 || ![[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text] || self.txtPassword.text.length<1 || self.txtConfirmPassword.text.length<1 || ![self.txtConfirmPassword.text isEqualToString:self.txtPassword.text] || self.txtMobile.text.length<1 || self.txtLicenseNumber.text.length<1)
        {
            if(self.txtName.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_NAME", nil)];
            }
            else if(self.txtUserName.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_USERNAME", nil)];
            }
            else if(self.txtEmail.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_EMAIL", nil)];
            }
            else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil)];
            }
            else if (self.txtPassword.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_PASSWORD", nil)];
            }
            else if (self.txtConfirmPassword.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_CONFIRM_PASSWORD", nil)];
            }
            else if (![self.txtConfirmPassword.text isEqualToString:self.txtPassword.text])
            {
                [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PASSWORD_NOT_MATCH",nil)];
            }
            else if (self.txtMobile.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_MOBILE", nil)];
            }
            else if (self.txtLicenseNumber.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_LICENCE_NUMBER", nil)];
            }
            /* else if (self.txtPlateNumber.text.length<1)
             {
             [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_PLATE_NUMBER", nil)];
             } */
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"REGISTERING", nil)];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:device_type forKey:PARAM_DEVICE_TYPE];
            [dictParam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
            [dictParam setObject:self.txtName.text forKey:PARAM_NAME];
            [dictParam setObject:self.txtUserName.text forKey:PARAM_USER_NAME];
            [dictParam setObject:self.txtEmail.text forKey:PARAM_EMAIL];
            [dictParam setObject:self.txtPassword.text forKey:PARAM_PASSWORD];
            [dictParam setObject:self.txtMobile.text forKey:PARAM_PHONE];
            [dictParam setObject:self.txtLicenseNumber.text forKey:PARAM_LICENCE];
            [dictParam setObject:@"" forKey:PARAM_PLATE];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            if (isProPicAdded)
            {
                UIImage *uploadImage=[[UtilityClass sharedObject]scaleAndRotateImage:self.imgProPic.image];
                [afn getDataFromPath:P_REGISTER withParamDataImage:dictParam andImage:uploadImage withBlock:^(id response, NSError *error)
                 {
                     if(response)
                     {
                         response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                         [[AppDelegate sharedAppDelegate] hideLoadingView];
                         if([[response valueForKey:@"success"] boolValue])
                         {
                             NSLog(@"register response ------ > %@",response);
                             
                             [NSPref SetPreference:PREF_ID Value:[response valueForKey:@"id"]];
                             [NSPref SetPreference:PREF_TOKEN Value:[response valueForKey:@"token"]];
                             self.viewForOtp.hidden=NO;
                         }
                         else
                         {
                             NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                             [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                         }
                     }
                 }];
            }
            else
            {
                [afn getDataFromPath:P_REGISTER withParamData:dictParam withBlock:^(id response, NSError *error) {
                    if(response)
                    {
                        response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                        [[AppDelegate sharedAppDelegate] hideLoadingView];
                        if([[response valueForKey:@"success"] boolValue])
                        {
                            NSLog(@"register response ------ > %@",response);
                            [NSPref SetPreference:PREF_ID Value:[response valueForKey:@"id"]];
                            [NSPref SetPreference:PREF_TOKEN Value:[response valueForKey:@"token"]];
                            self.viewForOtp.hidden=NO;
                        }
                        else
                        {
                            NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                            [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                        }
                    }
                    
                }];
            }
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

- (IBAction)onClickBtnOK:(id)sender
{
    [self performSegueWithIdentifier:REGISTER_SUCCESS sender:self];
}
- (IBAction)onClickBtnCheckBox:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag == 0)
    {
        btn.tag=1;
        [btn setBackgroundImage:[UIImage imageNamed:@"cb_glossy_on.png"] forState:UIControlStateNormal];
        self.btnRegister.enabled=TRUE;
        [self performSegueWithIdentifier:SEGUE_TO_TERMS sender:nil];
    }
    else
    {
        btn.tag=0;
        [btn setBackgroundImage:[UIImage imageNamed:@"cb_glossy_off.png"] forState:UIControlStateNormal];
        self.btnRegister.enabled=FALSE;
    }
    
}
- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnCamera:(id)sender
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing=YES;
        imagePickerController.view.tag = 0;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)];
    }
    
}
- (IBAction)onClickBtnGallery:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing=YES;
    imagePickerController.view.tag = 1;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
    }];
}

#pragma mark -
#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.imgProPic.contentMode = UIViewContentModeScaleAspectFill;
    self.imgProPic.clipsToBounds = YES;
    isProPicAdded=YES;
    self.imgProPic.image=[info objectForKey:UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - UITextFields Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.txtMobile)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
    }
    return YES;
}

@end
