//
//  SignInVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 24/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"

@interface SignInVC : BaseVC <UITextFieldDelegate>
{

}
@property (weak, nonatomic) IBOutlet UILabel *lblCopyRights;
@property (weak, nonatomic) IBOutlet UILabel *lblHaveAccount;
@property (weak, nonatomic) IBOutlet UILabel *lblNoWorries;
@property (weak, nonatomic) IBOutlet UILabel *lblMailInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblApproveMessage;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnForgetPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegisterHere;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseForForgetPasswordView;
@property (weak, nonatomic) IBOutlet UIButton *btnForgetPasswordSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnOkForApprove;

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtForgetPassword;


@property (weak, nonatomic) IBOutlet UIView *ViewForForgetPassword;
@property (weak, nonatomic) IBOutlet UIView *ViewForApproveMessage;


- (IBAction)onClickBtnBack:(id)sender;
- (IBAction)onClickBtnLogin:(id)sender;
- (IBAction)onClickBtnOkOfApproveMessage:(id)sender;
- (IBAction)onClickForgetPassword:(id)sender;
- (IBAction)OnClickBtnCloseForForgetPasswordView:(id)sender;
- (IBAction)OnClickBtnForgetPasswordSubmit:(id)sender;




@end
